package de.ostner.michael.zeiterfassung;

import java.nio.file.Path;
import java.nio.file.Paths;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import com.google.inject.name.Names;

import de.ostner.michael.zeiterfassung.bean.Eintrag;
import de.ostner.michael.zeiterfassung.concurrent.ListLoaderBuilder;
import de.ostner.michael.zeiterfassung.concurrent.impl.ListLoaderBuilderImpl;
import de.ostner.michael.zeiterfassung.concurrent.impl.ListWriterBuilderImpl;
import de.ostner.michael.zeiterfassung.controller.MainController;
import de.ostner.michael.zeiterfassung.controller.impl.ListWriterBuilder;
import de.ostner.michael.zeiterfassung.controller.impl.MainControllerImpl;
import de.ostner.michael.zeiterfassung.util.gson.EintragTypeAdapter;

public class MainModule extends AbstractModule
{

	@Override
	protected void configure()
	{
		bind(MainController.class).to(MainControllerImpl.class);
		bind(Path.class).annotatedWith(Names.named("EintragListPath")).toInstance(Paths.get("eintraege.json"));
		bind(ListWriterBuilder.class).to(ListWriterBuilderImpl.class);
		bind(ListLoaderBuilder.class).to(ListLoaderBuilderImpl.class);
	}

	@Provides
	public Gson getGson()
	{
		return new GsonBuilder().setPrettyPrinting().excludeFieldsWithoutExposeAnnotation().registerTypeAdapter(Eintrag.class, new EintragTypeAdapter()).create();
	}
}
