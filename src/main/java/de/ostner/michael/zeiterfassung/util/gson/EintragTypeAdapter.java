package de.ostner.michael.zeiterfassung.util.gson;

import java.io.IOException;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;

import de.ostner.michael.zeiterfassung.bean.Eintrag;

public class EintragTypeAdapter extends TypeAdapter<Eintrag>
{
	private static final DateTimeFormatter formatter = DateTimeFormatter.ISO_ZONED_DATE_TIME;

	@Override
	public void write(JsonWriter out, Eintrag value) throws IOException
	{
		out.beginObject();
		if (value.getKommen() != null)
		{
			out.name("kommen").value(formatter.format(value.getKommen()));
		}

		if (value.getGehen() != null)
		{
			out.name("gehen").value(formatter.format(value.getGehen()));
		}
		out.endObject();
	}

	@Override
	public Eintrag read(JsonReader in) throws IOException
	{
		ZonedDateTime kommen = null, gehen = null;
		in.beginObject();
		while (in.hasNext())
		{
			switch (in.nextName())
			{
			case "kommen":
				kommen = getZoneDateTime(in.nextString());
				break;
			case "gehen":
				gehen = getZoneDateTime(in.nextString());
				break;
			default:
				break;
			}
		}
		in.endObject();

		Eintrag returned = null;
		if (kommen != null || gehen != null)
		{
			returned = new Eintrag(kommen, gehen);
		}
		return returned;
	}

	private ZonedDateTime getZoneDateTime(String nextString)
	{
		return ZonedDateTime.from(formatter.parse(nextString));
	}

}
