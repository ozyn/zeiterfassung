package de.ostner.michael.zeiterfassung;

import com.google.inject.Guice;
import com.google.inject.Injector;

public class GuiceInjector
{
	private final static Injector injector = Guice.createInjector(new MainModule());

	public static <T> T getInstance(Class<? extends T> clazz)
	{
		return injector.getInstance(clazz);
	}
}
