package de.ostner.michael.zeiterfassung.concurrent;

import de.ostner.michael.zeiterfassung.bean.Eintrag;
import javafx.collections.ObservableList;

public interface ListLoaderBuilder
{
	public ListLoader build(ObservableList<Eintrag> listToAdd);
}
