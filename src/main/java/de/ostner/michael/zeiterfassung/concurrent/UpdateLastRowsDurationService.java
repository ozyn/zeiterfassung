package de.ostner.michael.zeiterfassung.concurrent;

import de.ostner.michael.zeiterfassung.bean.Eintrag;
import javafx.concurrent.ScheduledService;
import javafx.concurrent.Task;
import javafx.scene.control.TableView;

public class UpdateLastRowsDurationService extends ScheduledService<Void>
{
	private TableView<Eintrag> table;

	public UpdateLastRowsDurationService(TableView<Eintrag> table)
	{
		this.table = table;
	}

	@Override
	protected Task<Void> createTask()
	{
		return new UpdateLastRowsDurationTask(table);
	}

}
