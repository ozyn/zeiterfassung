package de.ostner.michael.zeiterfassung.concurrent.impl;

import java.nio.file.Path;
import java.util.List;

import com.google.gson.Gson;
import com.google.inject.Inject;
import com.google.inject.name.Named;

import de.ostner.michael.zeiterfassung.bean.Eintrag;
import de.ostner.michael.zeiterfassung.concurrent.ListWriter;
import de.ostner.michael.zeiterfassung.controller.impl.ListWriterBuilder;

public class ListWriterBuilderImpl implements ListWriterBuilder
{
	private final Gson gson;
	private final Path eintragListPath;

	@Inject
	public ListWriterBuilderImpl(Gson gson, @Named("EintragListPath") Path eintragListPath)
	{
		this.gson = gson;
		this.eintragListPath = eintragListPath;
	}

	@Override
	public ListWriter build(List<Eintrag> list)
	{
		return new ListWriterImpl(list, gson, eintragListPath);
	}

}
