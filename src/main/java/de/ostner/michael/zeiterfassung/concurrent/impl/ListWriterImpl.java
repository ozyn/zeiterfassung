package de.ostner.michael.zeiterfassung.concurrent.impl;

import java.io.BufferedWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;

import javafx.concurrent.Task;

import com.google.gson.Gson;

import de.ostner.michael.zeiterfassung.bean.Eintrag;
import de.ostner.michael.zeiterfassung.concurrent.ListWriter;

public class ListWriterImpl extends Task<Void> implements ListWriter
{
	private List<Eintrag> list;
	private Path eintragListPath;
	private Gson gson;

	public ListWriterImpl(List<Eintrag> list, Gson gson, Path eintragListPath)
	{
		this.list = list;
		this.gson = gson;
		this.eintragListPath = eintragListPath;
	}

	@Override
	protected Void call() throws Exception
	{
		System.out.printf("target: %s%n", eintragListPath.toAbsolutePath());
		try (BufferedWriter bufferedWriter = Files.newBufferedWriter(eintragListPath))
		{
			System.out.printf("writing %d entries to %s%n", list.size(), eintragListPath.toAbsolutePath());
			gson.toJson(list, bufferedWriter);
		} catch (Exception e)
		{
			e.printStackTrace();
			throw e;
		}
		return null;
	}
}
