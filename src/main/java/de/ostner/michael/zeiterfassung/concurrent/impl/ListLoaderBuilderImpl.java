package de.ostner.michael.zeiterfassung.concurrent.impl;

import java.nio.file.Path;

import javafx.collections.ObservableList;

import com.google.gson.Gson;
import com.google.inject.Inject;
import com.google.inject.name.Named;

import de.ostner.michael.zeiterfassung.bean.Eintrag;
import de.ostner.michael.zeiterfassung.concurrent.ListLoader;
import de.ostner.michael.zeiterfassung.concurrent.ListLoaderBuilder;

public class ListLoaderBuilderImpl implements ListLoaderBuilder
{

	private final Path eintragListPath;
	private final Gson gson;

	@Inject
	public ListLoaderBuilderImpl(Gson gson, @Named("EintragListPath") Path eintragListPath)
	{
		this.gson = gson;
		this.eintragListPath = eintragListPath;
	}

	@Override
	public ListLoader build(ObservableList<Eintrag> listToAdd)
	{
		return new ListLoaderImpl(eintragListPath, gson, listToAdd);
	}

}
