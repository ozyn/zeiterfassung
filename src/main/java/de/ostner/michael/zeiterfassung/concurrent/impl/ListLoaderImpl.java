package de.ostner.michael.zeiterfassung.concurrent.impl;

import java.io.BufferedReader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;

import javafx.collections.ObservableList;
import javafx.concurrent.Task;

import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;
import com.google.inject.Inject;
import com.google.inject.name.Named;

import de.ostner.michael.zeiterfassung.bean.Eintrag;
import de.ostner.michael.zeiterfassung.concurrent.ListLoader;

public class ListLoaderImpl extends Task<Void> implements ListLoader
{
	private final Path eintragListPath;
	private final Gson gson;
	private final ObservableList<Eintrag> listToAdd;

	@Inject
	public ListLoaderImpl(@Named("EintragListPath") Path eintragListPath, Gson gson, ObservableList<Eintrag> listToAdd)
	{
		this.eintragListPath = eintragListPath;
		this.gson = gson;
		this.listToAdd = listToAdd;
	}

	@Override
	protected Void call() throws Exception
	{
		System.out.printf("target: %s%n", eintragListPath.toAbsolutePath());
		try (BufferedReader bufferedReader = Files.newBufferedReader(eintragListPath))
		{
			@SuppressWarnings("serial")
			List<Eintrag> fromJson = gson.fromJson(bufferedReader, new TypeToken<List<Eintrag>>()
			{
			}.getType());

			if (fromJson != null)
			{
				System.out.printf("got %d entries from saved list%n", fromJson.size());
				listToAdd.addAll(fromJson);
			} else
			{
				System.out.println("fromJson was null. loaded nothing from " + eintragListPath.toAbsolutePath());
			}
		} catch (Exception e)
		{
			e.printStackTrace();
		}

		return null;
	}
}
