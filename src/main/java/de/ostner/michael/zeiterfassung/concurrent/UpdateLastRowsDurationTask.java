package de.ostner.michael.zeiterfassung.concurrent;

import java.time.Duration;
import java.time.ZonedDateTime;

import de.ostner.michael.zeiterfassung.bean.Eintrag;
import javafx.concurrent.Task;
import javafx.scene.control.TableView;

public class UpdateLastRowsDurationTask extends Task<Void>
{
	private final TableView<Eintrag> table;

	public UpdateLastRowsDurationTask(TableView<Eintrag> table)
	{
		this.table = table;
	}

	@Override
	protected Void call() throws Exception
	{
		try
		{
			Duration between = null;
			final int size = table.getItems().size();
			if (size > 0)
			{
				final Eintrag item = table.getItems().get(size - 1);
				if (item.getGehen() == null)
				{
					between = Duration.between(item.getKommen(), ZonedDateTime.now());
					item.setDifferenz(between);
				}
			}
			if (between == null)
			{
				System.out.println("called");
			} else
			{
				System.out.println(between);
			}
			return null;
		} catch (Exception e)
		{
			e.printStackTrace();
			throw e;
		}
	}
}
