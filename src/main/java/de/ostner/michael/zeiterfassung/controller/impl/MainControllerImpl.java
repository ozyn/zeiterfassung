package de.ostner.michael.zeiterfassung.controller.impl;

import java.net.URL;
import java.time.Duration;
import java.time.ZonedDateTime;
import java.util.ResourceBundle;

import javafx.collections.ListChangeListener;
import javafx.concurrent.ScheduledService;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableColumn.CellDataFeatures;
import javafx.scene.control.TableView;

import com.google.inject.Inject;

import de.ostner.michael.zeiterfassung.bean.Eintrag;
import de.ostner.michael.zeiterfassung.concurrent.ListLoaderBuilder;
import de.ostner.michael.zeiterfassung.concurrent.UpdateLastRowsDurationService;
import de.ostner.michael.zeiterfassung.controller.MainController;
import de.ostner.michael.zeiterfassung.gui.tableView.cell.DateCell;
import de.ostner.michael.zeiterfassung.gui.tableView.cell.DurationCell;
import de.ostner.michael.zeiterfassung.gui.tableView.cell.TimeCell;

public class MainControllerImpl implements MainController, Initializable
{
	@FXML
	private TableColumn<Eintrag, ZonedDateTime> columnDatum;
	@FXML
	private TableColumn<Eintrag, ZonedDateTime> columnKommen;
	@FXML
	private TableColumn<Eintrag, ZonedDateTime> columnGehen;
	@FXML
	private TableColumn<Eintrag, Duration> columnDifferenz;
	@FXML
	private TableView<Eintrag> table;

	private final ListLoaderBuilder listLoaderBuilder;
	private final ListWriterBuilder listWriterBuilder;

	@Inject
	public MainControllerImpl(ListLoaderBuilder listLoaderBuilder, ListWriterBuilder listWriterBuilder)
	{
		this.listLoaderBuilder = listLoaderBuilder;
		this.listWriterBuilder = listWriterBuilder;
	}

	@FXML
	public void kommen(ActionEvent event)
	{
		table.getItems().add(new Eintrag(ZonedDateTime.now(), null));
		updateList();
	}

	@FXML
	public void gehen(ActionEvent event)
	{
		Eintrag e = getLastEintrag();
		e.setGehen(ZonedDateTime.now());
		updateList();
	}

	private void updateList()
	{

	}

	private Eintrag getLastEintrag()
	{
		final int size = table.getItems().size();
		Eintrag e = null;
		if (size > 0)
		{
			e = table.getItems().get(size - 1);
			if (e.getGehen() != null)
			{
				e = null;
			}
		}

		if (e == null)
		{
			e = new Eintrag(null, null);
			table.getItems().add(e);
		}

		return e;
	}

	@Override
	public void initialize(URL location, ResourceBundle resources)
	{
		Thread thread = new Thread(listLoaderBuilder.build(table.getItems()));
		thread.setDaemon(true);
		thread.run();

		columnDatum.setCellValueFactory((CellDataFeatures<Eintrag, ZonedDateTime> p) -> p.getValue().kommenProperty());
		columnKommen.setCellValueFactory((CellDataFeatures<Eintrag, ZonedDateTime> p) -> p.getValue().kommenProperty());
		columnGehen.setCellValueFactory((CellDataFeatures<Eintrag, ZonedDateTime> p) -> p.getValue().gehenProperty());
		columnDifferenz.setCellValueFactory((CellDataFeatures<Eintrag, Duration> p) -> p.getValue().differenzProperty());

		columnDatum.setCellFactory((TableColumn<Eintrag, ZonedDateTime> p) -> new DateCell<Eintrag>());
		columnKommen.setCellFactory((TableColumn<Eintrag, ZonedDateTime> p) -> new TimeCell<Eintrag>());
		columnGehen.setCellFactory((TableColumn<Eintrag, ZonedDateTime> p) -> new TimeCell<Eintrag>());
		columnDifferenz.setCellFactory((TableColumn<Eintrag, Duration> p) -> new DurationCell<Eintrag>());

		ScheduledService<Void> t = new UpdateLastRowsDurationService(table);
		t.setRestartOnFailure(true);
		t.setDelay(javafx.util.Duration.seconds(1));
		t.setPeriod(javafx.util.Duration.seconds(1));
		t.start();

		table.getItems().addListener((ListChangeListener<Eintrag>) c -> {
			c.next();
			final int size = table.getItems().size();
			if (size > 0)
			{
				table.scrollTo(size - 1);
			}
		});

		final int size = table.getItems().size();
		if (size > 0)
		{
			table.scrollTo(size - 1);
		}
	}

	public void writeList()
	{
		new Thread(listWriterBuilder.build(table.getItems())).start();
	}
}
