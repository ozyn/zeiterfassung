package de.ostner.michael.zeiterfassung.controller.impl;

import java.util.List;

import de.ostner.michael.zeiterfassung.bean.Eintrag;
import de.ostner.michael.zeiterfassung.concurrent.ListWriter;

public interface ListWriterBuilder
{
	ListWriter build(List<Eintrag> list);
}
