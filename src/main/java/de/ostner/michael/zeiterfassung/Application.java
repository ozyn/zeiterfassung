package de.ostner.michael.zeiterfassung;

import de.ostner.michael.zeiterfassung.controller.MainController;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

public class Application extends javafx.application.Application
{

	public static void main(String[] args)
	{
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) throws Exception
	{
		primaryStage.setTitle("Zeiterfassung");

		final FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/view.fxml"));
		final MainController mainController = GuiceInjector.getInstance(MainController.class);

		fxmlLoader.setController(mainController);
		Parent myPane = fxmlLoader.load();

		primaryStage.setScene(new Scene(myPane));
		primaryStage.show();

		primaryStage.setOnCloseRequest(new EventHandler<WindowEvent>()
		{
			@Override
			public void handle(WindowEvent event)
			{
				mainController.writeList();
			}
		});
	}
}
