package de.ostner.michael.zeiterfassung.gui.tableView.cell;

import java.time.Duration;

import javafx.scene.control.TableCell;

public class DurationCell<T> extends TableCell<T, Duration>
{
	@Override
	protected void updateItem(Duration item, boolean empty)
	{
		super.updateItem(item, empty);

		if (item == null || empty)
		{
			this.setText(null);
		} else
		{
			long minutes = item.toMinutes();
			long hours = minutes / 60;
			minutes %= 60;

			this.setText(String.format("%02d:%02d", hours, minutes));
		}
	}
}
