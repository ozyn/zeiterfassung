package de.ostner.michael.zeiterfassung.gui.tableView.cell;

import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

import javafx.scene.control.TableCell;

public class TimeCell<T> extends TableCell<T, ZonedDateTime>
{
	private static final java.time.format.DateTimeFormatter formatter = DateTimeFormatter.ofPattern("HH:mm");

	@Override
	protected void updateItem(ZonedDateTime item, boolean empty)
	{
		super.updateItem(item, empty);

		if (item == null || empty)
		{
			this.setText(null);
		} else
		{
			this.setText(formatter.format(item));
		}
	}
}
