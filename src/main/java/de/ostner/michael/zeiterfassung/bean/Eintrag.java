package de.ostner.michael.zeiterfassung.bean;

import java.time.Duration;
import java.time.ZonedDateTime;

import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.value.ObservableValue;

import com.google.common.base.Objects;
import com.google.gson.annotations.Expose;

public class Eintrag
{
	@Expose private final ReadOnlyObjectWrapper<ZonedDateTime> kommen;
	@Expose private final ReadOnlyObjectWrapper<ZonedDateTime> gehen;
	private final ReadOnlyObjectWrapper<Duration> differenz;

	public Eintrag(final ZonedDateTime kommen, final ZonedDateTime gehen)
	{
		this.kommen = new ReadOnlyObjectWrapper<ZonedDateTime>();
		this.gehen = new ReadOnlyObjectWrapper<ZonedDateTime>();
		this.differenz = this.createDifferenzProperty(this.kommen, this.gehen);

		this.setKommen(kommen);
		this.setGehen(gehen);
	}

	private ReadOnlyObjectWrapper<Duration> createDifferenzProperty(final ReadOnlyObjectWrapper<ZonedDateTime> kommen, final ReadOnlyObjectWrapper<ZonedDateTime> gehen)
	{
		final ReadOnlyObjectWrapper<Duration> property = new ReadOnlyObjectWrapper<Duration>();
		kommen.addListener((ObservableValue<? extends ZonedDateTime> observable, ZonedDateTime oldValue, ZonedDateTime newValue) -> {
			if (newValue != null && gehen.get() != null)
				property.set(Duration.between(newValue, gehen.get()));
			else
				property.set(null);
		});
		gehen.addListener((ObservableValue<? extends ZonedDateTime> observable, ZonedDateTime oldValue, ZonedDateTime newValue) -> {
			if (newValue != null && kommen.get() != null)
				property.set(Duration.between(kommen.get(), newValue));
			else
				property.set(null);
		});
		return property;
	}

	public ZonedDateTime getKommen()
	{
		return kommen.get();
	}

	public void setKommen(ZonedDateTime kommen)
	{
		this.kommen.set(kommen);
	}

	public ZonedDateTime getGehen()
	{
		return gehen.get();
	}

	public void setGehen(ZonedDateTime gehen)
	{
		this.gehen.set(gehen);
	}

	public ReadOnlyObjectProperty<ZonedDateTime> kommenProperty()
	{
		return this.kommen.getReadOnlyProperty();
	}

	public ReadOnlyObjectProperty<ZonedDateTime> gehenProperty()
	{
		return this.gehen.getReadOnlyProperty();
	}

	public ReadOnlyObjectProperty<Duration> differenzProperty()
	{
		return this.differenz.getReadOnlyProperty();
	}

	@Override
	public String toString()
	{
		return Objects.toStringHelper(this).add("kommen", kommen.get()).add("gehen", gehen.get()).add("differenz", differenz.get()).toString();
	}

	public void setDifferenz(Duration duration)
	{
		differenz.set(duration);
	}

	public Duration getDifferenz()
	{
		return differenz.get();
	}
}
